class Pai < ApplicationRecord

  # RELACIONAMENTOS
  belongs_to :user, foreign_key: :user_id

  # VALIDATES
  validates :descricao, :presence => true, :uniqueness => true
  validates :sigla, :presence => true, :uniqueness => true
  validates :user_id, :presence => true

end
