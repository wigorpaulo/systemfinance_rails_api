class Api::V1::PaisController < Api::V1::ApiController
  before_action :set_pais, only: [:show, :update, :destroy]
  before_action :require_authorization!, only: [:show, :update, :destroy]

  # GET /api/v1/contacts
  def index
    # @pais = current_user.pais
    @pais = Pai.all
    render json: @pais
  end

  # GET /api/v1/contacts/1
  def show
    render json: @pais
  end

  # POST /api/v1/contacts
  def create
    @pais = Pai.new(pais_params.merge(user: current_user))

    if @pais.save
      render json: @pais, status: :created
    else
      render json: @pais.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /api/v1/contacts/1
  def update
    if @pais.update(pais_params)
      render json: @pais
    else
      render json: @pais.errors, status: :unprocessable_entity
    end
  end

  # DELETE /api/v1/contacts/1
  def destroy
    @pais.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_pais
    @pais = Pai.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def pais_params
    params.require(:pais).permit(:descricao, :sigla)
  end

  def require_authorization!
    unless current_user == pais.user
      render json: {}, status: :forbidden
    end
  end
end
