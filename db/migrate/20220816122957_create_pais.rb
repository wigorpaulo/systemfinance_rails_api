class CreatePais < ActiveRecord::Migration[6.1]
  def change
    unless table_exists? :pais
      create_table :pais do |t|
        t.string :descricao
        t.string :sigla
        t.references :user, null: false, foreign_key: true
        t.timestamps
      end
    end

    unless column_exists? :pais, :user_id
      add_column :pais, :user_id, :integer, foreign_key: true
    end

    if column_exists? :pais, :user_id
      unless foreign_key_exists? :pais, :users
        add_foreign_key :pais, :users, column: :user_id, primary_key: "id"
      end
    end
  end
end
